# wind-http

A library for HTTP communication, used by the wind browser.

## Status quo & plans

Right now we're relying on the [`http_req`](https://crates.io/crates/http_req) crate.
The goal is to rewrite it, to gain more control over the inner workings, but that's just anything but priority.

## Status

No active development