pub mod error;
use error::HTTPError;

use http_req::request;

pub fn request(url: &str) -> Result<Vec<u8>, HTTPError> {
    let mut writer = Vec::new(); // Container for body of a response
    let res = request::get(url, &mut writer).unwrap();

    match u16::from(res.status_code()) {
        200 => Ok(writer),
        status => Err(HTTPError {
            status_code: status,
        }),
    }
}
