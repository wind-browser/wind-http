use http_req::response::StatusCode;
use std::error::Error;
use std::fmt;

#[derive(Debug)]
pub struct HTTPError {
	pub status_code: u16,
}

impl Error for HTTPError {}

impl fmt::Display for HTTPError {
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
		let http_status = StatusCode::new(self.status_code);
		write!(
			f,
			"HTTP request returned non-200 result: {} {}.",
			self.status_code,
			http_status.reason().unwrap()
		)
	}
}
